require(tolRlink)
tolInit()

ShapeDefBase = tolEval('Include("ShapeDefBase.oza")')
for(s in 1:length(ShapeDefBase))
{
  for(i in 1:length(ShapeDefBase[[s]]))
  {
    for(j in 1:length(ShapeDefBase[[s]][[i]]))
    {
      #s=3;i=1;j=1
      len = length(ShapeDefBase[[s]][[i]][[j]]$Lag)
      if(!len) {
        ShapeDefBase[[s]][[i]][[j]] <- list (Lag=NULL,Coef=NULL)
      } else {
        ShapeDefBase[[s]][[i]][[j]]$Lag  <- as.numeric(ShapeDefBase[[s]][[i]][[j]]$Lag)
        ShapeDefBase[[s]][[i]][[j]]$Coef <- as.numeric(ShapeDefBase[[s]][[i]][[j]]$Coef)
      }
    }  
  }  
}  
saveRDS(ShapeDefBase,"ShapeDefBase.rds")

