##########################################################################
DoTsCleanAia <- function(zOrig,datingId)
##########################################################################
{
  cat('TRACE [DoTsCleanAia] \n')
  tm <-proc.time()
  showPlots=FALSE
  s = GetDatingPeriod(datingId)
  #z=zOrig
  #require(dygraphs)
  #dygraph(zOrig)
  z0 = zOrig*0
  mz = nrow(zOrig)
  zOrig.max = max(zOrig)
  zOrig.min = min(zOrig)
  zOrig.mean = mean(zOrig)
  zOrig.ratioZero = mean(zOrig==0)
  
  cummaxTrend.B = cummax(zOrig)
  lowTrend.B = cummaxTrend.B<zOrig.max/100
  lowStarting = sum(lowTrend.B)
  cummaxTrend.F = cummax(rev(zOrig))
  lowTrend.F = cummaxTrend.F<zOrig.max/100
  lowEnding = sum(lowTrend.F)
  zOrig.alive = zOrig[max(1,lowStarting):min(mz,mz-lowEnding+1)]
  mza=length(zOrig.alive)
  
  TransformDirect <- function(zor)
  {
    zor
   #log(100*zor/zOrig.mean+1)
  }  
  
  TransformInverse <- function(ztr)
  {
    ztr
   #(exp(ztr)-1)*zOrig.mean/100
  }  
  
  zTransf = TransformDirect(zOrig.alive)
  
  #deco0 = suppressWarnings(FastDecom(z=zTransf,s,doClean=FALSE, showPlots))
  # clean0 = TsClean(deco0$series$untrendcycle,s)
  # outlier0 = deco0$series$untrendcycle-clean0
 #dygraph(cbind(deco0$series$untrendcycle,outlier0))
 #dygraph(deco1$series[,c("original","trend","cycle")])
 #dygraph(deco1$series[,c("original","outlier")])

 #deco1 = suppressWarnings(FastDecom(deco0$series$untrendcycle,s,doClean=TRUE,showPlots+1))
  deco1 = suppressWarnings(FastDecom(z=zTransf,s, doClean=TRUE, showPlots))
  deco1.series = deco1$series
  #PlotDygraph(cbind(deco1.series$original,deco1.series$trendCycle))
  
  deco2 = suppressWarnings(FastDecom(deco1$series$cleanUntrendcycle,s,doClean=TRUE,showPlots))
  deco3 = suppressWarnings(FastDecom(deco2$series$cleanUntrendcycle,s,doClean=TRUE,showPlots))
  # sum(deco1$series$outlier!=0)
  # sum(deco2$series$outlier!=0)
  # sum(deco3$series$outlier!=0)
  outlier <- deco1$series$outlier + deco2$series$outlier + deco3$series$outlier
  beta <- outlier[outlier!=0]
  if(nrow(beta)) {
    sign <- ifelse(beta>=0,"Pos","Neg")
    cat('There are ',length(beta),' outliers\n',sep='')
    date = strftime(index(beta),"%Y-%m-%d %H:%M:%S")
    param=paste0("Pulse.",sign,".",strftime(index(beta),"%Y%m%d%H%M%S"))
    paramStats <- data.frame(
      type = 'Pulse',
      sign=sign, 
      date=date,
      param=param, 
      beta=abs(as.numeric(beta)),
      tStudent=NA,
      difLabel="")
    colnames(paramStats) <- c("type","sign","date","param","beta","tStudent","difLabel")
  } else {
    paramStats <-EmptyParamStats()
  }
  if(nrow(paramStats)) { rownames(paramStats) <- 1:nrow(paramStats) }
  clean = zOrig-outlier
  trend = deco1$series$trend + deco2$series$trend + deco3$series$trend
  cycle = deco1$series$cycle+deco2$series$cycle+deco3$series$cycle
  trendCycle = deco1$series$trendCycle+deco2$series$trendCycle+deco3$series$trendCycle
  colnames(clean) <- "clean"
  colnames(trend) <- "trend"
  colnames(cycle) <- "cycle"
  colnames(trendCycle) <- "trendCycle"
  
  series <- cbind(
    original = zOrig,
    outlier = outlier,
    clean = clean,
    trend = trend,
    cycle = cycle,
    trendCycle = trendCycle
  )
  colnames(series) <- c("original","outlier","clean","trend", "cycle", "trendCycle")
  if(!is.null(colnames(zOrig))) { colnames(series)[1] <- colnames(zOrig) }
  
  #dygraph(series$outlier)
  
  tm <-proc.time() -tm
  print(tm)
  result = list(
    paramStats=paramStats,
    series=series,
    method='TsCleanAia'
  )
  return(result)
}
  
##########################################################################
RunTsCleanAia <- function(jsonIn)
##########################################################################
{
  cat('TRACE [RunTsCleanAia] \n')
  tm <-proc.time()
  cJsonIn <- ConceptualJSON.FromJSON(jsonIn)
  dataIn <- cJsonIn$conceptual
  datingId <- cJsonIn$semistruct$output$`_TOL_dating`
  z <- dataIn$output
  result <- suppressWarnings(DoTsCleanAia(z,datingId))
 #result <- suppressWarnings(DoBayesAia(z,datingId))
  if(is.null(nrow(result$paramStats)))
  {
    stop("There are no outliers!")
  }
  semistruct <- list(
    paramStats = ConceptualJSON.data.frame2semistruct(result$paramStats),
    series = result$series  )
  cJsonOut <- ConceptualJSON.semistruct2conceptual(semistruct)
  jsonOut <- ConceptualJSON.ToJSON(cJsonOut)$json
  tm <-proc.time() -tm
  print(tm)
  return(jsonOut)
}