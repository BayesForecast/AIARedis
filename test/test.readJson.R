source("tools.R")
require(readr)
library(profvis)
options(scipen=999)

#minOutSig = 2 

#jsonIn <- read_file("../RTolMicroServRedis/test/testRedisClient.json")
#jsonIn <- read_file("../RTolMicroServRedis/test/audi_weekly.json")
#jsonIn <- read_file("../RTolMicroServRedis/test/audi_monthly.json")
jsonIn <- read_file("test/iphone.001.json")


 
tm <- proc.time()
#p <- profvis({


maxOutNum = 25
minOutSig = 3
allowedShapes = c("Compens","Pulse5R","Pulse4R",
  "Pulse3R","Pulse2R","Pulse2R21","Pulse2R12","Pulse")

cJsonIn <- ConceptualJSON.FromJSON(jsonIn)
dataIn <- cJsonIn$conceptual
minOutSig = dataIn$config$minOutSig
maxOutNum = dataIn$config$maxOutNum
allowedShapes = dataIn$config$allowedShapes


tol_dating <- cJsonIn$semistruct$output$`_TOL_dating`
if(tol_dating=='Daily') s=7
if(tol_dating=='Weekly') s=52
if(tol_dating=='Monthly') s=12
z <- dataIn$output

z0 = z*0
Z = as.numeric(z)
Z.d11 = SArima.Diff(Z,1,1,s)
z.d11 = xts(Z.d11, order.by = index(z)[(2+s):length(Z)])


paramStats <- NULL

AppendParamStats <- function(prmSts,difLabel)
{
  if(is.null(prmSts)) { return(0) }
  prmSts$difLabel <- difLabel
  if(is.null(paramStats)) { paramStats <<- prmSts }
  else { paramStats <<- rbind(paramStats, prmSts)}
  return(nrow(prmSts))
}

model.11 = FindSignifOutliersRecursive(u=z.d11,y=z.d11,d=1,D=1,s=s,
  minOutSig=minOutSig, maxOutNum=maxOutNum, allowedShapes=allowedShapes)

AppendParamStats(model.11$linReg$paramStats,"DifRegSea")

dr = polynomial(c(1,-1))
ds = ChangeBDegree(dr,s)
drs = dr*ds
dr2s = dr*drs

CalcIntegratedFilterAndNoise <- function(model,diffPol,degree)
{
  #model<-model.10
  if(is.null(model$linReg)){
    noise <- z
    filter <- z0 
  } else {
    noise <- z0 + c(Z[1:degree],
      SArima.RDifEq(as.numeric(model$noise),1,diffPol,numeric(),Z[1:degree]))
    filter <- z - noise
  }
  return(list(filter=filter, noise=noise))
}  

NF.d11 <- CalcIntegratedFilterAndNoise(model.11,drs,s+1)

decom <- FastTrendCycle(NF.d11$noise,s,dr)
trend = decom$trend

#ts_noise = ts(as.numeric(NF.d11$noise), frequency = s)
#decom_stl = stl(ts_noise, "periodic")
#trend = z0+as.numeric(decom_stl$time.series[,2])


if(F)
{
W.d11 = SArima.Diff(Z.d11,1,0,s)
w.d11 = xts(W.d11, order.by = index(z)[(3+s):length(Z)])

model.21 = FindSignifOutliersRecursive(u=w.d11,y=w.d11,d=1,D=1,
  minOutSig=minOutSig*2,  allowedShapes = c("Pulse"))
NF.d21 <- CalcIntegratedFilterAndNoise(model.11.step,dr2s,s+2)

  
ZUnSea <- as.numeric(decom$zUnSea)
ZUnSea.d10 <- SArima.Diff(ZUnSea,1,0,s)
zUnSea.d10 = xts(ZUnSea.d10, order.by = index(z)[(2):length(Z)])
  
model.10 = FindSignifOutliersRecursive(u=model.11$noise,y=zUnSea.d10,d=1,D=0,
  minOutSig=minOutSig,  allowedShapes = allowedShapes)

NF.d10 <- CalcIntegratedFilterAndNoise(model.10)

ZUnTrn <- as.numeric(decom$zUnTrn)
ZUnTrn.d01 <- SArima.Diff(ZUnTrn,0,1,s)
zUnTrn.d01 = xts(ZUnTrn.d01, order.by = index(z)[(s+1):length(Z)])

model.01 = FindSignifOutliersRecursive(model.11$noise,y=zUnTrn.d01,d=0,D=1,
  minOutSig=minOutSig,  allowedShapes = allowedShapes)

NF.d01 <- CalcIntegratedFilterAndNoise(model.01)

AppendParamStats(model.10$linReg$paramStats,"DifReg")
AppendParamStats(model.01$linReg$paramStats,"DifSea")
}

nonDup = !duplicated(paramStats$param)
paramStats <- paramStats[nonDup,]

dte <- strptime(paramStats$date,format='%Y%m%d',tz='UTC')
paramStats$date <- strftime(dte,format='%Y-%m-%d')
rownames(paramStats) <- 1:nrow(paramStats)

outlier = NF.d11$filter

series <- cbind(
  z,
  outlier,
  clean = z-outlier,
  trend = trend
)
names(series)[2:4]=c("outlier","clean","trend")
result = list(
  paramStats=paramStats,
  series=series
)

#cJsonOut <- ConceptualJSON.semistruct2conceptual(result)

#jsonOut <- ConceptualJSON.ToJSON(cJsonOut)$json


#})

tm <- proc.time()-tm
print(tm)


if(F)
{

print(p)
require(dygraphs)
dygraph(series)
dygraph(cbind(series,lag(z,s)))


dygraph(z.d11)

dygraph(cbind(z,NF.d11$filter,NF.d21$filter))
dygraph(cbind(NF.d11$noise))
dygraph(cbind(decom$zUnSea))
dygraph(cbind(decom$zUnTrn))

dygraph(cbind(z,decom$cycle,decom$trend))

dygraph(z)
dygraph(model.11$filter)
dygraph(model.11$noise)
dygraph(cbind(z,NF.d11$filter))
dygraph(cbind(z,NF.d11$noise))

dygraph(model.10$filter.y)
dygraph(model.10$filter)
dygraph(model.10$noise)
dygraph(cbind(z,NF.d01$filter))

dygraph(model.01$filter.y)
dygraph(model.01$filter)
dygraph(model.01$noise)

dygraph(cbind(NF.d10$filter))
dygraph(model.11$noise)
dygraph(zd-res)

}