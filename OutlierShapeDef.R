ShapeDefBase = readRDS("ShapeDefBase.rds")
##########################################################################
BuildOutlierShapeDef <- function(s, allowedShapes)
##########################################################################
{
  PosibleShapeDef = ShapeDefBase[[paste0("s",s)]]
  ShapeDef =  PosibleShapeDef[allowedShapes]
  for(j in 1:length(ShapeDef))
  {
    #j=1
    shapeDef = ShapeDef[[j]]
    type = names(ShapeDef)[j]
   #cat('TRACE [BuildOutlierShapeDef] j:',j,' type:',type,'\n')
    for(k in 1:length(shapeDef))
    {
      #k=1
      shd = shapeDef[[k]]
      dif = names(shapeDef)[k]
      numLag = length(shd$Lag)
     #cat('TRACE [BuildOutlierShapeDef]   k:',k,' type:',type,' dif:',dif,' numLag:',numLag,'\n')
      ShapeDef[[j]][[k]]$type <- type
      ShapeDef[[j]][[k]]$difLab <- dif
      ShapeDef[[j]][[k]]$numLag <- numLag
      ShapeDef[[j]][[k]]$maxLag <- NA
      if(numLag) { 
        ShapeDef[[j]][[k]]$maxLag <- max(shd$Lag) 
        ShapeDef[[j]][[k]]$invariant_byLags <- lapply(1:numLag,function(h)
        {
          #cat('TRACE [BuildOutlierShapeDef]     h:',h,'\n')
          values = shd$Coef[1:h]
          sumSqr = sum(values^2)
          list(
            numLag=h,
            lag = shd$Lag[1:h],
            coef = values,
            sumSqr = sumSqr,
            sqrtSumSqr = sqrt(sumSqr),
            weights = values/sumSqr
          )
        }) 
      }
    }
  }
  return(ShapeDef)
}  

