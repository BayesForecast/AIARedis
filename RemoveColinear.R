require(dplyr)
require(irlba)

#####################################################################################
DrawSparseColinearColumns <- function(
  maxRank=25,maxKernel=100,
  minRowFactor=2,maxRowFactor=50,
  minDens=0.05,maxDens=0.2,
  lowerCell=-1,upperCell=1)
#####################################################################################
{  
  #Draw the true rank of the design matrix
  trueRank = round(runif(1,2,maxRank))
  print(trueRank)
  #Draw the true number of colinear variables
  trueKernelDim = round(runif(1,2,maxKernel))
  #The total number of variables is the sum of both numbers
  ncol = trueRank+trueKernelDim
  #Draw the number of rows or data length
  nrow = round(runif(1,minRowFactor*ncol,maxRowFactor*ncol))
  #Draw the density of full rank part of design matrix
  density = runif(1,minDens,maxDens)
  #Draw full rank part of design matrix as Uniform in [-1,1]
  randomOne <- function(nnz) { runif(nnz,lowerCell,upperCell) }
  A0 = rsparsematrix(nrow, trueRank, density,rand.x=randomOne)
  #Draw the matrix of colinearity relations
  b1 = rsparsematrix(trueRank, trueKernelDim, sqrt(density),rand.x=randomOne)
  #Draw the colinear variables
  A1 = A0 %*% b1
  #Combine full rank and colinear variables
  A = cbind(A0,A1)
  #as.matrix(A)
  list(
    lowerCell = lowerCell,
    upperCell = upperCell,
    trueRank = trueRank,
    trueKernelDim = trueKernelDim,
    ncol = ncol,
    nrow = nrow,
    density = density,
    ncol = ncol,
    nrow = nrow,
    density = density,
    A = A
  )
}

#####################################################################################
BuildOrthogonalComplement <- function(V)
#Find orthogonal complement by columns of full rank matrix V with less columns than
#rows
#####################################################################################
{
  n = ncol(V)
  m = nrow(V)
  QRV = qr(V)
  K <- qr.Q(QRV,complete=TRUE)[,(n+1):m]
  return(K)
}  

#####################################################################################
RemoveColinearColumns <- function(A, checkFullRank=FALSE)
#####################################################################################
{  
  ncol = ncol(A)
  #Remove empty rows
  x = as.matrix(A[rowSums(A^2)>0,])
  #Calculate the Singular Value Decomposition
  SVD = svd(x)
  #Threshold to consider null the normalized eigenvalues in order to avoid
  #numerical rounding errores
  threshold = 1/(100*sum(x>0))
  #Normalized eigenvalues
  eigenNorm = SVD$d/max(SVD$d)
  #Estimation of the rank as number of normalized eigenvalues over threshold
  rank = sum(eigenNorm>threshold)
  #Estimation of the kernel dimension
  Kn = ncol-rank
  if(Kn==0) {
    removeIdx = c()
    Ac = A
  } else {
    #Kernel base: Right eigenvectors corresponding to near-zero eigenvalues
    if(ncol(SVD$v)==ncol) { K = SVD$v[1:ncol,(rank+1):ncol] 
    } else { K = BuildOrthogonalComplement(SVD$v[,1:rank]) }
    #Removing from kernel variables with all coefficients near to zer0
    if(Kn==1) { K = matrix(K,nrow=ncol,ncol=1)}
    K2S = arrange(data.frame(idx = 1:ncol, C2 = rowSums(K^2)),-C2)
    K2Ss = dplyr::summarize(dplyr::group_by(K2S,C2),count=dplyr::n())
    K2S = arrange(K2S,idx)
    idx1 = filter(K2S,C2>threshold)$idx
    #Clean kernel
    K1 = K[idx1,]
    #QR decomposition of clean kernel
    QRK1 = qr(t(K1))
    #Indexes of the natural base of kernel with minimum information
    removeIdx = idx1[QRK1$pivot[1:Kn]]
    Ac = A[,-removeIdx]
  }
  result = list(
    SVD = SVD,
    rank=rank,
    removeIdx=removeIdx,
    Ac = Ac
  )
  if(checkFullRank)
  {
   #Cheking that resting variables have full rank
    xc = x[,-removeIdx]
    QRC = qr(xc)
    print(rank)
    checkDim = diag(qr.R(QRC))
    #print(checkDim)
    rank_c = sum(abs(checkDim))
    print(rank_c)
    result = c(result,list(
      QRC=QRC,
      rank_c=rank_c
    ))
  }
  return(result)
}  

#####################################################################################
CalcBestAdjust <- function(y, SVD, rank)
#####################################################################################
{  
  oldBIC = Inf
  BIC = Inf
  bestRank = rank
  rk=rank
  while(rk>=0)
  {
    oldBIC <- BIC
    if(rk==0) {
      newResiduals <- y
      newStdErr <- sqrt(mean(newResiduals^2))
      BIC <- 2*my*log(newStdErr)+rk*log(my)
    } else {
      Di <- Diagonal(rk,(SVD$d[1:rk])^-1)
      Di2 <- Diagonal(rk,(SVD$d[1:rk])^-2)
      U <- as.matrix(SVD$u[,1:rk])
      V <- as.matrix(SVD$v[,1:rk])
      newBeta <- (V %*% (crossprod(U,y)/SVD$d[1:rk]))[1:rank,]
      newResiduals <- y-x%*%newBeta
      newStdErr <- sqrt(mean(newResiduals^2))
      BIC <- 2*length(y)*log(newStdErr)+rk*log(length(y))
    }
    cat('TRACE rank=',rk,' BIC=',BIC,' oldBIC=',oldBIC,'\n')
    if(BIC<oldBIC)
    {
      bestRank <- rk
      cat('TRACE bestRank=',bestRank,'\n')
      beta <- newBeta
      residuals <- newResiduals
      stdErr <- newStdErr
      VDi <- V %*% Di
      betaSigma <- stdErr*sqrt(rowSums(VDi^2)[1:nh])
      tStudent <- beta/betaSigma
    } else {
      break
    }
    rk <- rk-1
  }
  result = list(
    bestRank = bestRank,
    BIC = BIC
  )
}
    
    
#AS <- DrawSparseColinearColumns()
#RemoveColinearColumns(AS$A, checkFullRank=TRUE)
