require(rmarkdown)
RndSeed = round(runif(1,111111111,999999999))
system(paste0('tolsh -v -c"Real RndSeed=',RndSeed,'" ',
  'RandGenOutputWithOutliers.tol'))

output_path = 'report/AIASampler/'
dir.create(output_path,showWarnings=FALSE, recursive=TRUE)
output_file = paste0(output_path,'AIA.',
  strftime(Sys.time(),format='%Y%m%d%H%M%S'),'.',
  RndSeed,'.html')

rmarkdown::render("AIASampler.Rmd",encoding="UTF-8", 
    output_file  = output_file,
    params = list(RndSeed=RndSeed)
  )